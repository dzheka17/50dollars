<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::all();
        return view('admin.documents.index', compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'doc_name' => 'required|mimes:pdf,doc,docx'
        ]);

        $document = new Document($request->all());
        $document->is_act = $request->input('is_act') ? 1 : 0;

        if(isset($request['doc_name'])) {
            $file = $request->file('doc_name');
            $fname = substr(md5(microtime()), 0, 6);
            $path = $file->storeAs('docs', $fname . '.' . $file->getClientOriginalExtension());
            $document->doc = 'storage/' . $path;
        }
        $document->save();

        flash('Документ добавлен....', 'success');
        return redirect()->route('documents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);
        return view('admin.documents.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);
        return view('admin.documents.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        $document = Document::find($id);
        $document->update($request->all());
        if(isset($request['doc_name']))
        {
            $file = $request->file('doc_name');
            $fname = substr(md5(microtime()), 0, 6);
            $path = $file->storeAs('docs', $fname . '.' . $file->getClientOriginalExtension());
            $document->doc = 'storage/' . $path;
        }
        $document->save();
        flash('Документ обновлена....', 'success');
        return redirect()->route('documents.show', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::find($id);
        flash('Документ '. $document->title . ' удалена....', 'danger');
        if($document->doc) {
            $filename = $document->doc;
            Storage::delete(substr($filename, -15));
        }

        $document->destroy($id);
        return redirect(route('documents.index'));
    }
}
