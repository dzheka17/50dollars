<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(25);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'description' => 'required',
            'img_name' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $post = new Post($request->all());

        if(isset($request['img_name'])) {
            $file = $request->file('img_name');
            $fname = substr(md5(microtime()), 0, 6);
            $path = $file->storeAs('imgs', $fname . '.' . $file->getClientOriginalExtension());
            $post->photo = 'storage/' . $path;
        }
        $post->save();

        flash('Новость добавлена....', 'success');
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'description' => 'required',
            'img_name' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $post = Post::find($id);
        $post->update($request->all());
        if(isset($request['img_name']))
        {
            $file = $request->file('img_name');
            $fname = substr(md5(microtime()), 0, 6);
            $path = $file->storeAs('imgs', $fname . '.' . $file->getClientOriginalExtension());
            $post->photo = 'storage/' . $path;
        }
        $post->save();
        flash('Новость обновлена....', 'success');
        return redirect()->route('posts.show', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        flash('Новость '. $post->title . ' удалена....', 'danger');
        if($post->photo ) {
            $filename = $post->photo;
            Storage::delete(substr($filename, -15));
        }

        $post->destroy($id);
        return redirect(route('posts.index'));
    }
}
