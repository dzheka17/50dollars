<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::orderBy('created_at', 'decs')->paginate(30);
        return view('admin.messages.index', compact('messages'));
    }

    public function show($id)
    {
        $message = Message::findOrFail($id);
        $message->is_new = 0;
        $message->save();
        return view('admin.messages.show', compact('message'));
    }

    public function destroy($id)
    {
        $post = Message::find($id);
        flash('Сообщение удалено', 'danger');
        $post->destroy($id);
        return redirect(route('messages.index'));
    }
}
