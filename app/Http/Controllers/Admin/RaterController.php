<?php

namespace App\Http\Controllers\Admin;

use App\Rater;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class RaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $raters = Rater::all();
        return view('admin.raters.index', compact('raters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.raters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:raters|max:255',
            'body' => 'required',
            'description' => 'required',
            'img_name' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $rater = new Rater($request->all());

        if(isset($request['img_name']))
        {
            $file = $request->file('img_name');
            $fname = substr(md5(microtime()), 0, 6);
            $path = $file->storeAs('ratr', $fname . '.' . $file->getClientOriginalExtension());
            $rater->photo = 'storage/' . $path;
        }
        $rater->save();

        flash('Оценщик добавлен....', 'success');
        return redirect()->route('raters.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rater = Rater::findOrFail($id);
        return view('admin.raters.show', compact('rater'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rater = Rater::findOrFail($id);
        return view('admin.raters.edit', compact('rater'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|unique:raters|max:255',
            'body' => 'required',
            'description' => 'required',
            'img_name' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $rater = Rater::find($id);
        $rater->update($request->all());
        if(isset($request['img_name']))
        {
            $file = $request->file('img_name');
            $fname = substr(md5(microtime()), 0, 6);
            $path = $file->storeAs('imgs', $fname . '.' . $file->getClientOriginalExtension());
            $rater->photo = 'storage/' . $path;
        }
        $rater->save();
        flash('Оценщик обновлен....', 'success');
        return redirect()->route('raters.show', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rater = Rater::find($id);
        flash('Оценщик '. $rater->title . ' удален....', 'danger');
        if($rater->photo ) {
            $filename = $rater->photo;
            Storage::delete(substr($filename, -15));
        }

        $rater->destroy($id);
        return redirect(route('raters.index'));
    }
}
