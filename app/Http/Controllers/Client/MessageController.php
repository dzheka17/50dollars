<?php

namespace App\Http\Controllers\Client;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function store(Request $request)
    {
        Message::create($request->all());
        $msg = "Ваше сообщение отправлено";
        flash($msg, 'success');
        return redirect()->route('client.contacts');
    }
}
