<?php

namespace App\Http\Controllers\Client;

use App\Post;
use App\Rater;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($search = $request->input('search')) {
            $raters = Rater::orWhere('title', 'like', '%' . $search. '%')
                ->orWhere('description', 'like', '%' . $search. '%')
                ->orWhere('body', 'like', '%' . $search. '%')
                ->orderBy('title')->paginate(6);
        } else {
            $raters = Rater::orderBy('title')->paginate(6);
        }
        return view('client.raters.index', compact('raters', 'search'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rater = Rater::findOrFail($id);
        $raterKey = 'rater_' . $id;
        if (!Session::has($raterKey)) {
            Rater::where('id', $id)->increment('views');
            Session::put($raterKey, 1);
        }
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        return view('client.raters.show', compact('rater', 'popular_posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
