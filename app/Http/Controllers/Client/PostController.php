<?php

namespace App\Http\Controllers\Client;

use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($search = $request->input('search')) {
            $posts = Post::orWhere('title', 'like', '%' . $search. '%')
                ->orWhere('description', 'like', '%' . $search. '%')
                ->orWhere('body', 'like', '%' . $search. '%')
                ->orderBy('created_at', 'desc')->paginate(5);
        } else {
            $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        }
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        return view('client.posts.index', compact('posts', 'search', 'popular_posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        $post = Post::findOrFail($id);

        $blogKey = 'blog_' . $id;
        if (!Session::has($blogKey)) {
            Post::where('id', $id)->increment('views');
            Session::put($blogKey, 1);
        }
        return view('client.posts.show', compact('post', 'popular_posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
