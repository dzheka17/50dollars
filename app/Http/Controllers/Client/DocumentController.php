<?php

namespace App\Http\Controllers\Client;

use App\Document;
use App\Http\Controllers\Controller;
use App\Post;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::limit(10)->orderBy('title')->paginate(10);
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        return view('client.documents.index', compact('popular_posts', 'documents'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        return view('client.documents.show', compact('document', 'popular_posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
