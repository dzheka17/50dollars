<?php

namespace App\Http\Controllers\Client;

use App\Post;
use App\Vacancy;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class VacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::all();
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        return view('client.vacancies.index', compact('vacancies', 'popular_posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vacancy = Vacancy::findOrFail($id);
        $raterKey = 'vacancy_' . $id;
        if (!Session::has($raterKey)) {
            Vacancy::where('id', $id)->increment('views');
            Session::put($raterKey, 1);
        }
        $popular_posts = Post::limit(4)->orderBy('views', 'desc')->get();
        return view('client.vacancies.show', compact('vacancy', 'popular_posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
