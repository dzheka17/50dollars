<?php

namespace App\Http\Controllers;

use App\Post;
use App\Rater;
use App\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        $posts = Post::limit(3)->orderBy('created_at', 'desc')->get();
        return view('index', compact('services', 'posts'));
    }
}
