<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'title',
        'doc',
        'body',
        'description',
    ];

    protected $appends = [
        'doc_link'
    ];

    public function link()
    {
        return $this->doc ? env('APP_URL') . '/' . $this->doc : '';
    }

    public function getDocLinkAttribute()
    {
        return $this->link();
    }
}
