<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'description',
        'photo',
        'body',
        'views',
    ];

    protected $appends = [
        'photo_link'
    ];

    public function link()
    {
        return $this->photo ? env('APP_URL') . '/' . $this->photo : '';
    }

    public function getPhotoLinkAttribute()
    {
        return $this->link();
    }

}
