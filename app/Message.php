<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'email',
        'name',
        'text',
    ];

    public static function isNew()
    {
        return Message::where('is_new', 1)->count();
    }

}
