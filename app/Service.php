<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'title',
        'icon',
        'body',
    ];

    protected $appends = [
        'fa_icon'
    ];

    public function faIcon()
    {
        return 'fa '. $this->icon;
    }

    public function getFaIconAttribute()
    {
        return $this->faIcon();
    }
}
