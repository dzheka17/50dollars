<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rater extends Model
{
    protected $fillable = [
        'title',
        'photo',
        'description',
        'body',
    ];

    protected $appends = [
        'photo_link'
    ];

    public function link()
    {
        return $this->photo ? env('APP_URL') . '/' . $this->photo : '';
    }

    public function getPhotoLinkAttribute()
    {
        return $this->link();
    }
}
