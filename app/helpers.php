<?php

function active_if_routes($routes, $prefix = null)
{
    // get current route name
    $current_route = Request::route()->getName();

    if (is_array($routes)) {
        foreach ($routes as $route) {
            if (!is_null($prefix)) $route = $prefix . $route;
            if (str_is($route . '*', $current_route)) return 'active';
        }
        return '';
    }

    if (str_is($routes . '*', $current_route)) return 'active';
    return '';
}

function set_active_sidebar($routes, $prefix = null)
{
    // get current route name
    $current_route = Request::route()->getName();

    if (is_array($routes)) {
        foreach ($routes as $route) {
            if (!is_null($prefix)) $route = $prefix . $route;
            if (str_is($route . '*', $current_route)) return 'active';
        }
        return '';
    }

    if (str_is($routes . '*', $current_route)) return 'active';
    return '';
}