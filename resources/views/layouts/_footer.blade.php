<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="widget">
                    <h5 class="widgetheading">Наши контакты</h5>
                    <address>
                        <strong>AC RT company Inc</strong><br>
                        734025, Таджикистан, Душанбе,<br>
                        ул. Бохтар 37/1
                        <br>
                        <i class="icon-phone"></i> Тел: +992 90 703 2322 <br>
                        <i class="icon-envelope-alt"></i> e-mail: aort.tj@gmail.com
                    </address>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget">
                    <h5 class="widgetheading">Страницы</h5>
                    <ul class="link-list">
                        <li><a href="/">Главная</a></li>
                        <li><a href="#">Наши услуги</a></li>
                        <li><a href="{{ route('client.vacancies.index') }}">Вакансии</a></li>
                        <li><a href="{{ route('client.contacts') }}">Контакты</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget">
                    <h5 class="widgetheading">Другие новости</h5>
                    <ul class="link-list">
                        <li>
                            <a target="_blank" href="http://news.tj/ru/news/tajikistan/society/20170818/v-tadzhikistane-sozdana-assotsiatsiya-otsentshikov">
                                В Таджикистане создана Ассоциация оценщиков
                            </a>
                        </li>
                        <li>
                            <a href="http://www.vault.com/company-rankings/consulting/vault-consulting-50" target="_blank">
                                2018 Vault Consulting 50
                            </a>
                        </li>
                        <li>
                            <a href="http://www.ocenchik.ru/docse/2124-ocenshchik-eto-prizvanie.html" target="_blank">
                                Оценщик - это призвание
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>

    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>
                            <span>
                                &copy; Ассоциация оценщиков Республики Таджикистан @php echo date("Y"); @endphp. Все права защищены.
                            </span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="social-network">
                        <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
                        <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
                        <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin fa-1x"></i></a></li>
                        <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest fa-1x"></i></a></li>
                        <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus fa-1x"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>