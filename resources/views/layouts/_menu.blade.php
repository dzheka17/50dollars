<header>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div class="navbar-brand">
                            <a href="/">
                                <img src="{{ asset('logo_full.png') }}"
                                     class="logo"
                                     width="45px"
                                     height="45px"
                                     style="max-width:100px; margin-top: -15px;">
                            </a>
                        </div>
                    </div>
                    <div class="menu">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="{{ set_active_sidebar('home')}}"><a href="/">Главная</a></li>
                            <li role="presentation" class="{{ set_active_sidebar('client.about')}}"><a href="{{ route('client.about') }}">О нас</a></li>
                            {{--<li class="dropdown">--}}
                                {{--<a href="#"--}}
                                   {{--class="dropdown-toggle {{ set_active_sidebar('client.about.')}}"--}}
                                   {{--data-toggle="dropdown"--}}
                                   {{--role="button"--}}
                                   {{--aria-expanded="true"--}}
                                   {{--aria-haspopup="true">--}}
                                   {{--О нас--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu">--}}
                                    {{--<li><a href="#">Цели и задачи деятельности ассоциации</a></li>--}}
                                    {{--<li><a href="#">Наши услуги</a></li>--}}
                                    {{--<li><a href="#">Членство в ассоциации. Ассоциативные члены</a></li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            <li role="presentation" class="{{ set_active_sidebar('client.raters.')}}"><a href="{{ route('client.raters.index') }}">Реестр оценщиков</a></li>
                            <li role="presentation" class="{{ set_active_sidebar('client.documents.')}}"><a href="{{ route('client.documents.index') }}">Законы</a></li>
                            <li role="presentation" class="{{ set_active_sidebar('client.posts.')}}"><a href="{{ route('client.posts.index') }}">Новости</a></li>
                            <li role="presentation" class="{{ set_active_sidebar('client.education')}}"><a href="{{ route('client.education') }}">Обучение</a></li>
                            <li role="presentation" class="{{ set_active_sidebar('client.vacancies.')}}"><a href="{{ route('client.vacancies.index') }}">Вакансии</a></li>
                            <li role="presentation" class="{{ set_active_sidebar('client.contacts')}}" ><a href="{{ route('client.contacts') }}">Контакты</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>