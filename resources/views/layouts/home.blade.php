<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'АО РТ') }}{{ $page_title or '' }}</title>

    <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>
    {{-- Styles --}}
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive-slider.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @stack('stylesheets')
</head>
<body>
{{-- Top bar --}}
@include('layouts._menu')

@yield('content')

{{-- Footer --}}
@include('layouts._footer')

{{-- Scripts --}}
<script src="{{ asset('js/jquery.js') }}"></script>
{{-- Custom--}}
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/responsive-slider.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script>
    wow = new WOW({}).init();
</script>
@stack('scripts')
</body>
</html>