@extends('layouts.home', [
    'page_title' => " - Вакансии"
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="recent">
                <button class="btn-primarys"><h3>Вакансии</h3></button>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="page-header">
                    @foreach($vacancies as $key => $vacancy)
                        @include('client.vacancies._vacancy')
                    @endforeach
                    @if($vacancies->count() == 0)
                        @include('client._partial._empty')
                    @endif
                </div>
            </div>

            <div class="col-md-4">
                @include('client.posts.popular')
            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>
@endsection