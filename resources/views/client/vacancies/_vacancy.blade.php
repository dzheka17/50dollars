<div class="blog wow fadeInUp" data-wow-delay="{{ ($key  + 1) * 0.2 }}s">
    <h5>{{ \Carbon\Carbon::parse($vacancy->created_at)->format('d.m.Y') }}</h5>
    <h3>{{ $vacancy->title }}</h3>
    <p>{{ $vacancy->description }}</p>
    <div class="ficon">
        <a href="{{ route('client.vacancies.show', $vacancy->id) }}" alt="">Подробнее</a> <i class="fa fa-long-arrow-right"></i>
    </div>
    <hr>
</div>