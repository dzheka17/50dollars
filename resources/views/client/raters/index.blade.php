@extends('layouts.home', [
    'page_title' => " - Реестр оценщиков"
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="recent">
                <form class="form-search pull-right">
                    <input name="search" class="form-control" type="text" placeholder="Поиск.." value="{{$search or ''}}">
                    @if(isset($search))
                        <span class="text-danger">
                            Показаны результаты по запросу <strong class="text-info">"{{ $search }}"</strong>
                            <br>
                            <a class="pull-right" href="{{ route('client.raters.index') }}">Сбросить</a>
                        </span>
                    @endif
                </form>
                <button class="btn-primarys"><h3>Реестр оценщиков</h3></button>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            @foreach($raters as $key => $rater)
                 @include('client._partial._rater')
            @endforeach
            @if($raters->count() == 0)
                @include('client._partial._empty')
            @endif
        </div>
    </div>

    <div class="container">
        {{ $raters->links() }}
    </div>


    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>
@endsection