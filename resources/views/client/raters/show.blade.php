@extends('layouts.home', [
    'page_title' => " - " . $rater->title
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="recent">
                <button class="btn-primarys"><h3>{{$rater->title}}</h3></button>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="page-header wow fadeInUp" data-wow-delay="0.2s">
                    <div class="blog">
                        <h5>{{ \Carbon\Carbon::parse($rater->created_at)->format('d.m.Y') }}</h5>
                        <img src="{{ $rater->photo_link }}" class="img-responsive" alt=""/>
                        <h3>{{ $rater->title }}</h3>
                        <p>{{ $rater->description }}</p>
                        <div>{!! $rater->body !!}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                @include('client.posts.popular')
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>
@endsection