@extends('layouts.home')

@section('content')
<div class="container">
    <div class="row">
        <div class="recent">
            <button class="btn-primarys"><h3>О нас</h3></button>
            <hr>
            <div class="blog wow fadeInUp text-left" data-wow-delay="0.3s">
                <h5>
                <pre>
  Ассоциация оценщиков Республики Таджикистана (далее - «Ассоциация»), создана юридическими фирмами, имеющими лицензию на осуществление оценочной деятельности
в Республике таджикистан, при поддержке USAID.

  Ассоциация является добровольным объединением юридических лиц, филиалов и представительств юридических лиц-нерезидентов, специализированных финансово-
кредитных учреждений, действующих на территории Республики Таджикистан, иные некоммерческие организации (общественные объединения, ассоциации, союзы), а также
иных организаций независимо от форм собственности, признающих требования настоящего Устава и уплачивающих членские взносы.

  Ассоциация образована на неопределенный срок и осуществляет свою деятельность в соответствии с Конституцией Республики Таджикистан, Гражданским кодексом
Республики Таджикистан, иными нормативными правовыми актами Республики Таджикистан, настоящим Уставом, а также внутренними документами Ассоциации.
                </pre>
                </h5>
            </div>
            <br>
            <div class="blog wow fadeInUp text-left" data-wow-delay="0.6s">
                <h5 class="text-center"><b>ЦЕЛИ И ЗАДАЧИ ДЕЯТЕЛЬНОСТИ АССОЦИАЦИИ</b></h5>
                <h5>
                    <pre>
  Ассоциация создана в целях обеспечения взаимодействия организаций, предоставляющих услуги по оценке в Республике Таджикистан при решении общих организацион-
ных, правовых и технических вопросов, возникающих в деятельности указанных организаций, защиты и представления их общих законных интересов перед государствен-
ными органами Республики Таджикистан, государственными и частными организациями, учреждениями и предприятиями, в том числе иностранными, а также оказания со-
действия развитию отрасли оказания услуг по оценке в Республике Таджикистан.

 <u>Ассоциация выполняет следующие задачи:</u><ul> <li>защита и представление общих интересов членов Ассоциации перед государственными органами, государственными и частными предприятиями, учреждениями и орга-
низациями;</li> <li>оказание содействия членам Ассоциации в установлении и поддержании межрегиональных и международных связей с оценочными организациями других стран;</li> <li>оказание консультативных и методических услуг членам Ассоциации на платной или бесплатной основе;</li> <li>содействие членам Ассоциации в установлении и поддержании связей с соответствующими органами государственной власти и управления Республики Таджикистан,
исполнительными органами государственной власти на местах, и заинтересованными организациями, учреждениями любых форм собственности;</li> <li>обеспечение и оказание содействия в осуществлении постоянной связи между членами Ассоциации;</li> <li>организация семинаров, конференции, встреч и иных мероприятий по обсуждению насущных проблем оценочной деятельности в Республике Таджикистан, выработка
практических решений различных вопросов, стоящих перед членами Ассоциации;</li> <li>объединение и координация деятельности членов Ассоциации по совершенствованию нормативно-правовой базы и развитию оценочной деятельности в Республике
Таджикистан;</li> <li>удовлетворение информационных потребностей и профессиональных интересов членов Ассоциации оценщиков;</li> <li>разработка, обсуждение и лоббирование законодательных и нормативных актов, затрагивающих интересы членов Ассоциации, внесение предложений по ним в соот-
ветствующие органы государственной власти и управления, экспертиза законопроектов, взаимодействие с их разработчиками;</li> <li>развитие оценочной деятельности в Республике Таджикистан, участие в мероприятиях, проводимых органами государственной власти и управления по развитию
и совершенствованию оценочной деятельности;</li> <li>оказание членам Ассоциации организационной, информационно-аналитической, методической, правовой и иной помощи;</li> <li>повышение авторитета, надежности и взаимного доверия между членами Ассоциации, обеспечение соблюдения членами Ассоциации этических принципов деятельности
оценщиков, расширение делового партнерства, а также контактов между руководителями и специалистами членов Ассоциации;</li> <li>обеспечение репутационных и правовых гарантий деятельности членов Ассоциации, содействие их независимому развитию и безопасности;</li> <li>расширение сотрудничества членов Ассоциации с зарубежными организациями оценщиков и ассоциациями, международными финансовыми организациями.</li></ul>
 <u>В соответствии с возложенными задачами Ассоциация:</u><ul> <li>представляет и защищает интересы членов Ассоциации в органах государственной власти и управления, международных и других учреждениях и организациях;</li> <li>добивается принятия законодательных и нормативных актов, стимулирующих развитие оценочной деятельности в Республике Таджикистан, участвует в разработке
и обсуждении проектов указанных актов, экспертизе проектов законов и иных нормативных правовых актов, направляет свои заключения в соответствующие зако-
нодательные и исполнительные органы государственной власти Республики Таджикистан, осуществляет подготовку предложений по совершенствованию оценочной
деятельности, порядка расчетов налогооблагаемой базы, участвует в решении других вопросов, связанных с организацией деятельности оценщиков, малого и
среднего бизнеса;</li> <li>осуществляет информационно-методическое обеспечение членов Ассоциации по основным направлениям их деятельности;</li> <li>организует профессиональную подготовку и повышение квалификации оценщиков и специалистов иных организаций, учреждений и объединений, а также проводит
обучение и иные различные мероприятия для учащихся ВУЗов в целях подготовки специалистов в области оценки;</li> <li>организует стажировку оценщиков внутри страны и за рубежом;</li> <li>консультирует членов Ассоциации, оказывает методическую и практическую помощь с учетом отечественного и зарубежного опыта по методологическим вопросам
оценки, управленческим, финансовым, правовым и другим вопросам, возникающим в их деятельности;</li> <li>организует для членов Ассоциации обмен опытом работы оценщиков и оказывает помощь в установлении деловых контактов путем проведения конференций,
консультаций, "круглых столов", семинаров, тренингов и др.;</li> <li>изучает конъюнктуру и тенденции развития экономики, оценочной деятельности, банковского дела, финансового рынка, развития предпринимательства и инфор-
мирует по этим вопросам членов Ассоциации;</li> <li>проводит маркетинговые и аналитические исследования в целях создания информационной базы для оценщиков - членов Ассоциации;</li> <li>организует освещение в средствах массовой информации деятельность оценщиков и Ассоциации, их роли в экономической жизни Республики Таджикистан;</li> <li>осуществляет деятельность по оценке качества услуг, оказываемых оценщиками или оценочными организациями;</li> <li>выполняет иные функции, отвечающие целям и задачам деятельности Ассоциации и не противоречащие законодательству Республики Таджикистан.</li>
                        </ul>
                    </pre>
                </h5>
            </div>
            <div class="blog wow fadeInUp text-left" data-wow-delay="0.9s">
                <h5 class="text-center"><b>ЧЛЕНСТВО В АССОЦИАЦИИ.</b></h5>
                <h5>
                    <pre>
 Членами Ассоциации могут быть коммерческие организации, осуществляющие деятельность в области предоставления услуг по оценке и совместно с услугами по оцен-
ке - иных сопутствующих услуг в Республике Таджикистан, финансово-кредитные учреждения, действующие на территории Республики Таджикистан, иные некоммерческие
организации (общественные объединения, ассоциации, союзы), международные организации и их представительства.
                    </pre>
                </h5>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <hr>
    </div>
</div>
@endsection