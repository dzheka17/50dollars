<!--о нас-->
<div class="container">
    <div class="about">
        <div class="row">
            <div class="recent">
                <button class="btn-primarys"><h3>О нас</h3></button>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="row-slider">
                <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.2s">
                    <div class="col-lg-6 mar-bot30">
                        <br>
                        <br>
                        <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                            <div class="slides" data-group="slides">
                                <ul>
                                    <div class="slide-bodys" data-group="slide">
                                        <li><img alt="" class="img-responsive" src="{{ asset('img/logo.jpg') }}" width="100%" height="450"/></li>
                                        <li><img alt="" class="img-responsive" src="img/1.jpg" width="100%" height="450"/></li>
                                        <li><img alt="" class="img-responsive" src="img/10.jpg" width="100%" height="450"/></li>
                                        <li><img alt="" class="img-responsive" src="img/2a.jpg" width="100%" height="450"/></li>
                                    </div>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                        <div class="thumnails">
                            <p>
                                <strong>
                                    Ассоциация оценщиков Республики Таджикистан, создана юридическими
                                    фирмами, имеющими лицензию на осуществление оценочной деятельности в Республике Таджикистан,
                                    при поддержке USAID.
                                </strong>
                            </p>
                            {{--<hr>--}}
                            <p>
                                <strong>
                                    Ассоциация является добровольным объединением юридических лиц,
                                    филиалов и представительств юридических лиц-нерезидентов, специализированных
                                    финансово-кредитных учреждений, действующих на территории Республики Таджикистан,
                                    иные некоммерческие организации (общественные объединения, ассоциации, союзы),
                                    а также иных организаций независимо от форм собственности,
                                    признающих требования настоящего Устава и уплачивающих членские взносы.
                                {{--<hr>--}}
                                {{--Ассоциация образована на неопределенный срок и осуществляет свою деятельность в соответствии--}}
                                    {{--с Конституцией Республики Таджикистан, Гражданским кодексом Республики Таджикистан, иными--}}
                                    {{--нормативными правовыми актами Республики Таджикистан, настоящим Уставом, а также--}}
                                    {{--внутренними документами Ассоциации.--}}
                                </strong>
                            </p>
                            <div class="ficon">
                                <a href="{{ route('client.about') }}" alt="">Подробнее</a> <i class="fa fa-long-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>