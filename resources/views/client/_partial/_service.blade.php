<div class="col-md-4">
    <div class="wow flipInY" data-wow-offset="0" data-wow-delay="{{ ($key + 1) * 0.2 }}s">
        <div class="align-center">
            <h4>{{ $service->title }}</h4>
            <div class="icon">
                <i class="{{ $service->fa_icon }} fa-4x"></i>
            </div>
            <p>{{ $service->body }}</p>
        </div>
    </div>
</div>