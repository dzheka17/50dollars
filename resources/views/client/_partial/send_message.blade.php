<div class="recent">
    <button class="btn-primarys"><h3>Отправьте сообщение</h3></button>
    @include('flash::message')
</div>
{!! Form::open(['route' => 'client.messages.store', 'method' => 'POST']) !!}
<div class="form-group">
    <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Ваше имя" required>
</div>

<div class="form-group">
    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Введите эл. адрес" required>
</div>
<textarea class="form-control" name="text" rows="8"></textarea>
<button type="submit" class="btn btn-default">Отправить</button>
{!! Form::close() !!}