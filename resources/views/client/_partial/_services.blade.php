<div class="container">
    <div class="row">
        <div class="recent">
            <button class="btn-primarys"><h3>Наши услуги</h3></button>
            <hr>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="content">
            @foreach($services as $key => $service)
                @include('client._partial._service')
            @endforeach
        </div>
    </div>
</div>