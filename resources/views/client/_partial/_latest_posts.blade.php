<div class="container">
    <div class="row">
        <div class="recent">
            <button class="btn-primarys">
                <h3>Последние новости</h3>
            </button>
            <hr>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="content">
            @foreach($posts as $key => $post)
                @include('client._partial._post')
            @endforeach
        </div>
    </div>
</div>