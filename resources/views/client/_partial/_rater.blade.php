<div class="col-md-4">
    <div class="wow fadeInUp" data-wow-delay="{{ ($key  + 1) * 0.2 }}s">
        <div class="align-center">
            <h4>{{ $rater->title }}</h4>
            <img src="{{ $rater->photo_link }}" alt="" class="img-responsive">
            <p>
                {!! $rater->description !!}
            </p>
            <div class="ficon">
                <a href="{{ route('client.raters.show', $rater->id) }}" alt="">Подробнее</a>
                <i class="fa fa-long-arrow-right"></i>
            </div>
        </div>
    </div>
</div>