<div class="col-md-4">
    <div class="wow fadeInUp" data-wow-delay="{{ ($key + 1) * 0.2 }}s">
        <div class="align-center">
            <h4>{{ $post->title }}</h4>
            <img src="{{ $post->photo_link }}" alt="" class="img-responsive">
            <p>{{ $post->description }}</p>
            <div class="ficon">
                <a href="{{ route('client.posts.show', $post->id) }}" alt="">Подробнее</a>
                <i class="fa fa-long-arrow-right"></i>
                <br>
                <small>{{ \Carbon\Carbon::parse($post->created_at)->format('d.m.Y') }}</small>
            </div>
        </div>
    </div>
</div>