@extends('layouts.home', [
    'page_title' => " - " . $post->title
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="recent">
                <button class="btn-primarys"><h3>{{$post->title}}</h3></button>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="page-header wow fadeInUp" data-wow-delay="0.2s">
                    <div class="blog">
                        <h5>
                            <small style="margin-right: 15px" class="pull-right text-info">Просмотры {{ $post->views }}</small>
                            {{ \Carbon\Carbon::parse($post->created_at)->format('d.m.Y') }}
                        </h5>
                        <img src="{{ $post->photo_link }}" class="img-responsive" alt="{{ $post->title }}"/>

                        <h3>{{ $post->title }}</h3>

                        <p>{{ $post->description }}</p>

                        <div>
                            {!! $post->body !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                @include('client.posts.popular')
            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>
@endsection