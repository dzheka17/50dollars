<div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Популярные новости</strong>
        </div>
        @foreach($popular_posts as $post)
        <div class="panel-body">
            <div class="media">
                <a class="media-left" href="#">
                    <img src="{{ $post->photo_link }}" alt="" width="50px">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">{{ $post->title }}</h4>
                    <p>{{$post->description}}</p>
                    {{--<br>--}}
                    {{--<small class="text-info">Просмотры: {{ $post->views }}</small>--}}
                    {{--<br>--}}
                    <div class="ficon">
                        <a href="{{ route('client.posts.show', $post->id) }}" alt="">Подробнее</a> <i class="fa fa-long-arrow-right"></i>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        @endforeach
    </div>
</div>