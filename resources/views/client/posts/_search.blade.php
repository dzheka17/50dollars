<form class="form-search">
    <input name="search" class="form-control" type="text" placeholder="Поиск.." value="{{$search or ''}}">
    @if($search)
        <span class="text-danger">
            <a class="pull-right" href="{{ route('client.posts.index') }}">Сбросить</a>
            Показаны результаты по запросу <strong class="text-info">"{{ $search }}"</strong>
        </span>
    @endif
</form>