<div class="blog wow fadeInUp" data-wow-delay="{{ ($key  + 1) * 0.2 }}s">
    <h5>{{ \Carbon\Carbon::parse($post->created_at)->format('d.m.Y') }}</h5>
    <img src="{{ $post->photo_link }}" class="img-responsive" alt=""/>
    <h3>{{ $post->title }}</h3>
    <p>{{ $post->description }}</p>
    <div class="ficon">
        <a href="{{ route('client.posts.show', $post->id) }}" alt="">Подробнее</a> <i class="fa fa-long-arrow-right"></i>
    </div>
    <hr>
</div>