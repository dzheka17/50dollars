@extends('layouts.home', [
    'page_title' => " - Новости"
])

@section('content')
<div class="container">
    <div class="row">
        <div class="recent">
            <button class="btn-primarys"><h3>Новости</h3></button>
            <hr>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-8">
            <div class="page-header">
                @foreach($posts as $key => $post)
                    @include('client.posts._post')
                @endforeach
                @if($posts->count() == 0)
                    @include('client._partial._empty')
                @endif
            </div>
        </div>

        <div class="col-md-4">
            @include('client.posts._search')
            @include('client.posts.popular')
        </div>

    </div>
</div>

<div class="container">
    {{ $posts->links() }}
</div>

<div class="container">
    <div class="row">
        <hr>
    </div>
</div>
@endsection