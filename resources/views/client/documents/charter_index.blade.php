@extends('layouts.home', [
    'page_title' => " - Документы"
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="recent">
                <button class="btn-primarys"><h3>Документы</h3></button>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="page-header">
                    @foreach($documents as $key => $document)
                        @include('client.documents._document')
                    @endforeach
                    @if($documents->count() == 0)
                        @include('client._partial._empty')
                    @endif
                </div>
            </div>

            <div class="col-md-4">
                @include('client.posts.popular')
            </div>

        </div>
    </div>

    <div class="container">
        {{ $documents->links() }}
    </div>

    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>
@endsection