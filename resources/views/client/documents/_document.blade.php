<div class="blog wow fadeInUp" data-wow-delay="{{ ($key  + 1) * 0.2 }}s">
    <a href="{{ $document->doc_link }}"
       style="margin-right: 15px"
       class="btn-link pull-right"
       title="Открыть документ"
       target="_blank" download>
        <i class="fa fa-file-pdf-o fa-3x"></i>
    </a>
    <h3>{{ $document->title }}</h3>
    <p>{{ $document->description }}</p>
    {{--<div class="ficon">--}}
        {{--<a href="{{ route('client.documents.show', $document->id) }}" alt="">Подробнее</a> <i class="fa fa-long-arrow-right"></i>--}}
    {{--</div>--}}
    <hr>
</div>