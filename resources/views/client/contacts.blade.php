@extends('layouts.home')

@section('content')
<div class="container">
    <div class="row">
        <div class="recent">
            <button class="btn-primarys"><h3>Контакты</h3></button>
            <hr>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3119.104223593229!2d68.78757581464433!3d38.57744767334612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38b5d15e55d5efa3%3A0xf53bc0f8f5d70815!2z0JLQtdGE0LAg0KbQtdC90YLRgA!5e0!3m2!1sru!2s!4v1521693037956" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

            {{--ключ AIzaSyB3EB_L-8ZVx9eNyeKGI8UIk2uob8B7qsM--}}
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            @include('client._partial.send_message')
        </div>

        <div class="col-lg-6">
            <div class="recent">
                <button class="btn-primarys"><h3>Свяжитесь с нами</h3></button>
            </div>
            <div class="contact-area">
                <h4>Адрес:</h4> ул. Бохтар 37/1, Душанбе<br>
                <h4>Телефон:</h4>+992 90 703 2322</br>
                <h4>E-mail:</h4>aort.tj@gmail.com</br>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <hr>
    </div>
</div>
@endsection