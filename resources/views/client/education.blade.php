@extends('layouts.home')

@section('content')
<div class="container">
    <div class="row">
        <div class="recent">
            <button class="btn-primarys"><h3>Обучение</h3></button>
            <hr>
            <div class="blog wow fadeInUp text-center" data-wow-delay="0.3s">
                <h5>
                    <p>
                        В данном разделе. Вы можете ознакомиться с расписанием курсов, семинаров и тренингов,
                        проводимых Ассоциацией Оценщиков Республики Таджикистан, а так же нашими партнерами.
                    </p>
                </h5>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <hr>
    </div>
</div>
@endsection