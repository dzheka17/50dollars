<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'АССОЦИАЦИЯ ОЦЕНЩИКОВ РТ') }}</title>
    {{--<link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('css/admin/cosmo.bootstrap.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/admin/sand.bootstrap.min.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('css/admin/cerulian.bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}" target="_blank">
                        <i class="fa fa-home"></i>
                        АО РТ
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav">
                        @guest
                        @else
                        <li class="{{ set_active_sidebar('posts.') }}">
                            <a href="{{ route('posts.index') }}">
                                <i class="fa fa-bars"></i>
                                Новости
                            </a>
                        </li>
                        <li class="{{ set_active_sidebar('vacancies.') }}">
                            <a href="{{ route('vacancies.index') }}">
                                <i class="fa fa-wrench"></i>
                                Вакансии
                            </a>
                        </li>
                        <li class="{{ set_active_sidebar('documents.') }}">
                            <a href="{{ route('documents.index') }}">
                                <i class="fa fa-file-o"></i>
                                Документы
                            </a>
                        </li>
                        <li class="{{ set_active_sidebar('raters.') }}">
                            <a href="{{ route('raters.index') }}">
                                <i class="fa fa-bar-chart-o"></i>
                                Реест оценщиков
                            </a>
                        </li>
                        <li class="{{ set_active_sidebar('services.') }}">
                            <a href="{{ route('services.index') }}">
                                <i class="fa fa-paperclip"></i>
                                Услуги
                            </a>
                        </li>
                        @endguest
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="{{ set_active_sidebar('messages.') }}">
                                <a href="{{ route('messages.index') }}">
                                    <i class="fa fa-envelope"></i>
                                    @if(\App\Message::isNew() > 0)
                                    <small title="Новые сообщения">
                                        <span class="label label-danger">{{ \App\Message::isNew() }}</span>
                                    </small>
                                    @endif
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('users.index') }}">
                                            <i class="fa fa-cog"></i>
                                            Сменить пароль
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i>
                                            Выход
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @include('flash::message')

        @yield('content')
    </div>
    <script src="{{ asset('js/admin/app.js') }}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
    @section('scripts')
        @stack('scripts')
    @show
</body>
</html>
