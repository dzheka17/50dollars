@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('documents.create') }}" class="pull-right btn btn-xs btn-primary">
                        Добавить
                    </a>
                    Документы
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Doc</th>
                        <th>time</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $document)
                        <tr>
                            <td>
                                <a href="{{ route('documents.show', $document->id) }}">
                                    {{ $document->title }}
                                </a>
                            </td>
                            <td>{{ $document->description }}</td>
                            <td>
                                <a href="{{ $document->doc_link }}" target="_blank" download>
                                    Документ
                                    <i class="fa fa-download"></i>
                                </a>
                            </td>
                            <td>{{ $document->created_at }}</td>
                            <td class="text-right">
                                <a href="{{ route('documents.edit', $document->id) }}" class="btn btn-xs btn-warning">
                                    Изменить
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
