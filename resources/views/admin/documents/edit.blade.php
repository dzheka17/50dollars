@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $document->title }}
                </div>
                <div class="panel-body">

                    {!! Form::model($document, ['route' => ['documents.update', $document->id], 'method' => 'PUT', 'files'=>'true']) !!}
                    @include('admin.documents._form')

                    <a href="{{ $document->doc_link }}" download>Документ</a>

                    <div class="form-group text-center">
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary btn-lg']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
