@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="javascript:;"
                       class="btn btn-xs btn-danger pull-right"
                       onclick="deleteTest({{ $document->id }})">
                        Удалить
                    </a>
                    <a style="margin-right: 15px" href="{{ route('documents.edit', $document->id) }}" class="btn btn-xs btn-warning pull-right">
                        Изменить
                    </a>
                    {{ $document->title }}
                    <form id="delete-test-{{ $document->id }}" action="{{ route('documents.destroy', $document->id) }}" method="post" style="display: none;">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <h1>
                        {{ $document->title }}
                    </h1>
                    <hr>
                    <a href="{{ $document->doc_link }}" download>Документ</a>
                    <p>
                        {{ $document->description }}
                    </p>
                    {!! $document->body !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function deleteTest(id) {
        if (confirm('Вы действительно хотите удалить?')){
            event.preventDefault();
            document.getElementById('delete-test-' + id).submit();
        }
    }
</script>
@endpush

