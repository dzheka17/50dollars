@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="{{ $service->fa_icon }}"></i>
                    {{ $service->title }}
                </div>
                <div class="panel-body">

                    {!! Form::model($service, ['route' => ['services.update', $service->id], 'method' => 'PUT']) !!}
                    @include('admin.services._form')

                    <div class="form-group text-center">
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary btn-lg']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
