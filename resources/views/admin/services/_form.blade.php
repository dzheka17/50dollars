<div class="form-group">
    {!! Form::label('title', 'Название:') !!}
    {!! Form::text('title', null, [
            'class' => 'form-control',
            'required'
        ]) !!}
    @if($errors->has('title'))
        <span class="help-block">{{ $errors->first('title') }}</span>
    @endif
</div>

<div class="form-group">
    {!! Form::label('icon', 'Иконка:') !!}
    {!! Form::text('icon', null, [
            'class' => 'form-control',
            'required'
        ]) !!}
    @if($errors->has('icon'))
        <span class="help-block">{{ $errors->first('icon') }}</span>
    @endif
</div>

<div class="form-group">
    <label>Текст</label>
    {{ Form::textarea('body', null, ['class' => 'form-control', 'required']) }}
    @if($errors->has('body'))
        <span class="help-block">{{ $errors->first('body') }}</span>
    @endif
</div>
