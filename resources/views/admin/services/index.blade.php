@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('services.create') }}" class="pull-right btn btn-xs btn-primary">
                        Добавить
                    </a>
                    Услуги
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>icon</th>
                        <th>time</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>
                                <a href="{{ route('services.show', $service->id) }}">
                                    {{ $service->title }}
                                </a>
                            </td>
                            <td>
                                <i class="{{ $service->fa_icon }}"></i>
                            </td>
                            <td>{{ $service->created_at }}</td>
                            <td class="text-right">
                                <a href="{{ route('services.edit', $service->id) }}" class="btn btn-xs btn-warning">
                                    Изменить
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
