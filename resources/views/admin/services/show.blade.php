@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="javascript:;"
                       class="btn btn-xs btn-danger pull-right"
                       onclick="deleteTest({{ $service->id }})">
                        Удалить
                    </a>
                    <a style="margin-right: 15px" href="{{ route('services.edit', $service->id) }}" class="btn btn-xs btn-warning pull-right">
                        Изменить
                    </a>
                    <i class="{{ $service->fa_icon }}"></i>
                    {{ $service->title }}
                    <form id="delete-test-{{ $service->id }}" action="{{ route('services.destroy', $service->id) }}" method="post" style="display: none;">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <h1>
                        <i class="{{ $service->fa_icon }}"></i>
                        {{ $service->title }}
                    </h1>
                    <hr>
                    {!! $service->body !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function deleteTest(id) {
        if (confirm('Вы действительно хотите удалить?')){
            event.preventDefault();
            document.getElementById('delete-test-' + id).submit();
        }
    }
</script>
@endpush

