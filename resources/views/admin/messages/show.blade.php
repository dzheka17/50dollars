@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="javascript:;"
                       class="btn btn-xs btn-danger pull-right"
                       onclick="deleteTest({{ $message->id }})">
                        Удалить
                    </a>
                    Сообщение
                    <form id="delete-test-{{ $message->id }}" action="{{ route('messages.destroy', $message->id) }}" method="post" style="display: none;">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <h1>
                        {{ $message->name }}
                    </h1>
                    <h4>
                        {{ $message->email }}
                    </h4>
                    <hr>
                    {{ $message->text }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function deleteTest(id) {
        if (confirm('Вы действительно хотите удалить?')){
            event.preventDefault();
            document.getElementById('delete-test-' + id).submit();
        }
    }
</script>
@endpush

