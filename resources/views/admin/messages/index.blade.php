@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Сообщения
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>email</th>
                        <th></th>
                        <th>time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td>
                                <a href="{{ route('messages.show', $message->id) }}">
                                    {{ $message->name }}
                                </a>
                            </td>
                            <td>{{ $message->email }}</td>
                            <td>
                                @if($message->is_new == 1)
                                    <span class="label label-danger">New</span>
                                @else
                                @endif
                            </td>
                            <td>{{ $message->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $messages->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
