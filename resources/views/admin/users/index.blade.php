@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Сменить пароль</div>
                <div class="panel-body">

                    {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update', $user->id],'class' => 'form-horizontal']) !!}

                    <div class="form-group {{ isset($error) ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label">
                            Текущий пароль
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-4">
                            {!! Form::password('old_password', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => '', 'required']) !!}
                            @if($errors->has('old_password'))
                                <span class="help-block text-danger">{{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label">
                            Новый пароль
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-4">
                            {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => '', 'required']) !!}
                            @if($errors->has('password'))
                                <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">
                            Повторите пароль
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-4">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => '', 'required']) !!}
                            @if($errors->has('password_confirmation'))
                                <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-4">
                            {!! Form::submit('Изменить пароль', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
