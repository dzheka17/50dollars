<div class="form-group">
    {!! Form::label('title', 'Название:') !!}
    {!! Form::text('title', null, [
            'class' => 'form-control',
            'required'
        ]) !!}
    @if($errors->has('title'))
        <span class="help-block">{{ $errors->first('title') }}</span>
    @endif
</div>

<div class="form-group">
    <label>Превью</label>
    {{ Form::textarea('description', null, ['class' => 'form-control']) }}
    @if($errors->has('description'))
        <span class="help-block">{{ $errors->first('description') }}</span>
    @endif
</div>

<div class="form-group">
    <label>Текст</label>
    {{ Form::textarea('body', null, ['id' => 'article-ckeditor']) }}
    @if($errors->has('body'))
        <span class="help-block">{{ $errors->first('body') }}</span>
    @endif
</div>

<hr>

<div class="form-group">
    {!! Form::label('img_name', 'Фото: (jpeg,png,jpg,gif,svg)') !!}
    {!! Form::file('img_name', ['class' => 'form-control']) !!}
    @if($errors->has('img_name'))
        <span class="help-block">{{ $errors->first('img_name') }}</span>
    @endif
</div>