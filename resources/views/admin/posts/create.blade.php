@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Добавить новость
                </div>
                <div class="panel-body">

                    {!! Form::open(['route' => 'posts.store', 'method' => 'POST', 'files'=>'true']) !!}
                    @include('admin.posts._form')

                    <div class="form-group text-center">
                        {!! Form::submit('Создать', ['class' => 'btn btn-primary btn-lg']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
