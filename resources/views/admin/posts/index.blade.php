@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('posts.create') }}" class="pull-right btn btn-xs btn-primary">
                        Добавить
                    </a>
                    Новости
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>views</th>
                        <th>time</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>
                                <img src="{{ $post->photo_link }}" alt="" height="30px">
                            </td>
                            <td>
                                <a href="{{ route('posts.show', $post->id) }}">
                                    {{ $post->title }}
                                </a>
                            </td>
                            <td>
                                {{ $post->description }}
                            </td>
                            <td>
                                {{ $post->views }}
                            </td>
                            <td>{{ $post->created_at }}</td>
                            <td class="text-right">
                                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-xs btn-warning">
                                    Изменить
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
