@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $post->title }}
                </div>
                <div class="panel-body">

                    <div class="img-rounded">
                        <img src="{{ $post->photo_link }}" height="200">
                    </div>

                    {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT', 'files'=>'true']) !!}
                    @include('admin.posts._form')

                    <div class="form-group text-center">
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary btn-lg']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
