@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="javascript:;"
                       class="btn btn-xs btn-danger pull-right"
                       onclick="deleteTest({{ $vacancy->id }})">
                        Удалить
                    </a>
                    <a style="margin-right: 15px" href="{{ route('vacancies.edit', $vacancy->id) }}" class="btn btn-xs btn-warning pull-right">
                        Изменить
                    </a>
                    {{ $vacancy->title }}
                    <form id="delete-test-{{ $vacancy->id }}" action="{{ route('vacancies.destroy', $vacancy->id) }}" method="post" style="display: none;">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <h1>
                        {{ $vacancy->title }}
                    </h1>
                    <div class="img-rounded">
                        <img src="{{ $vacancy->photo }}" alt=""  height="400">
                    </div>
                    <hr>
                    {!! $vacancy->body !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function deleteTest(id) {
        if (confirm('Вы действительно хотите удалить?')){
            event.preventDefault();
            document.getElementById('delete-test-' + id).submit();
        }
    }
</script>
@endpush

