@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('vacancies.create') }}" class="pull-right btn btn-xs btn-primary">
                        Добавить
                    </a>
                    Вакансии
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th></th>
                        <th>views</th>
                        <th>time</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vacancies as $post)
                        <tr>
                            <td>
                                <a href="{{ route('vacancies.show', $post->id) }}">
                                    {{ $post->title }}
                                </a>
                            </td>
                            <td>{{ $post->description }}</td>
                            <td>{{ $post->views }}</td>
                            <td>{{ $post->created_at }}</td>
                            <td class="text-right">
                                <a href="{{ route('vacancies.edit', $post->id) }}" class="btn btn-xs btn-warning">
                                    Изменить
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
