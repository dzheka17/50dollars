<div class="form-group">
    {!! Form::label('title', 'Название:') !!}
    {!! Form::text('title', null, [
            'class' => 'form-control',
            'required'
        ]) !!}
    @if($errors->has('title'))
        <span class="help-block">{{ $errors->first('title') }}</span>
    @endif
</div>
<div class="form-group">
    <label>Описание</label>
    {{ Form::textarea('description', null, ['class' => 'form-control']) }}
    @if($errors->has('description'))
        <span class="help-block">{{ $errors->first('description') }}</span>
    @endif
</div>
<div class="form-group">
    <label>Текст</label>
    {{ Form::textarea('body', null, ['id' => 'article-ckeditor', 'required']) }}
    @if($errors->has('body'))
        <span class="help-block">{{ $errors->first('body') }}</span>
    @endif
</div>
