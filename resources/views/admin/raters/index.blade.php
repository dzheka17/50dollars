@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('raters.create') }}" class="pull-right btn btn-xs btn-primary">
                        Добавить
                    </a>
                    Реестр оценщиков
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>views</th>
                        <th>time</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($raters as $rater)
                        <tr>
                            <td>
                                <img src="{{ $rater->photo_link }}" alt="" height="30px">
                            </td>
                            <td>
                                <a href="{{ route('raters.show', $rater->id) }}">
                                    {{ $rater->title }}
                                </a>
                            </td>
                            <td>
                                {{ $rater->views }}
                            </td>
                            <td>{{ $rater->created_at }}</td>
                            <td class="text-right">
                                <a href="{{ route('raters.edit', $rater->id) }}" class="btn btn-xs btn-warning">
                                    Изменить
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
