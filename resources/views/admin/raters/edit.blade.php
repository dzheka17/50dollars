@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $rater->title }}
                </div>
                <div class="panel-body">

                    <div class="img-rounded">
                        <img src="{{ $rater->photo_link }}" alt=""  height="400">
                    </div>

                    {!! Form::model($rater, ['route' => ['raters.update', $rater->id], 'method' => 'PUT', 'files'=>'true']) !!}
                    @include('admin.raters._form')

                    <div class="form-group text-center">
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary btn-lg']) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
