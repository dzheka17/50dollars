@extends('layouts.home')

@section('content')
    @include('client._partial._slider')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contents">
                    <h2>Официальный сайт Ассоциации оценщиков Республики Таджикистан</h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>

    @include('client._partial._about')

    @include('client._partial._services')

    @include('client._partial._latest_posts')

    <div class="container">
        <div class="row">
            <hr>
        </div>
    </div>
@endsection