<?php


Route::get('/', 'HomeController@index')->name('home');
Route::group([
    'as' => 'client.',
    'namespace' => 'Client',
    ], function () {

    Route::resource('/vacancies', 'VacancyController', ['only' => ['index', 'show']]);
    Route::resource('/posts', 'PostController', ['only' => ['index', 'show']]);
    Route::resource('/raters', 'RaterController', ['only' => ['index', 'show']]);
    Route::resource('/messages', 'MessageController', ['only' => ['store']]);

    Route::get('/contacts', function () {
        return view('client.contacts');
    })->name('contacts');

    Route::get('/about', function () {
        return view('client.about');
    })->name('about');

    Route::get('/education', function () {
        return view('client.education');
    })->name('education');

    Route::resource('/documents', 'DocumentController', ['only' => ['index', 'show']]);
//    Route::group(['prefix' => 'documents'], function () {
//        Route::get('/', 'DocumentController@index')->name('documents.index');
//        Route::get('/{id}', 'DocumentController@show')->name('documents.show')->where('id', '[0-9]+');
//        Route::get('/charter', 'DocumentController@charter_index')->name('documents.charter.index');
//        Route::group(['prefix' => 'charter'], function () {
//            Route::get('/{id}', 'DocumentController@charter_show')->name('documents.charter.show');
//        });
//    });

});

Auth::routes();

Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth',
    'namespace' => 'Admin'
], function () {

    Route::get('/', 'AdminController@index')->name('admin');
    Route::resource('/users', 'UserController');
    Route::resource('/documents', 'DocumentController');
    Route::resource('/vacancies', 'VacancyController');
    Route::resource('/posts', 'PostController');
    Route::resource('/raters', 'RaterController');
    Route::resource('/services', 'ServiceController');
    Route::resource('/messages', 'MessageController');
});

